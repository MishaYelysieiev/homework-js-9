const getFullAge = () => {
    let today = new Date();
    let birthday = prompt("Enter your birthday in dd.mm.yyyy"," ").split(".");
    
    let age = today.getMonth()+1 >= birthday[1] ? today.getFullYear() - +(birthday[2]) : today.getFullYear() - +(birthday[2]) -1;
    
    alert("You are "+age+" years old");
    
    let zodiac;
    
    if(birthday[0]>=22 && birthday[1]==12 || birthday[0]<=20 && birthday[1]==1) {
        zodiac = "Capricorn";
    };
    if(birthday[0]>=21 && birthday[1]==1 || birthday[0]<=19 && birthday[1]==2) {
        zodiac = "Aquarius";
    };
    if(birthday[0]>=20 && birthday[1]==2 || birthday[0]<=20 && birthday[1]==3) {
        zodiac = "Pisces";
    };
    if(birthday[0]>=21 && birthday[1]==3 || birthday[0]<=20 && birthday[1]==4) {
        zodiac = "Aries";
    };
    if(birthday[0]>=21 && birthday[1]==4 || birthday[0]<=20 && birthday[1]==5) {
        zodiac = "Taurus";
    };
    if(birthday[0]>=21 && birthday[1]==5 || birthday[0]<=21 && birthday[1]==6) {
        zodiac = "Gemini";
    };
    if(birthday[0]>=22 && birthday[1]==6 || birthday[0]<=22 && birthday[1]==7) {
        zodiac = "Cancer";
    };
    if(birthday[0]>=23 && birthday[1]==7 || birthday[0]<=23 && birthday[1]==8) {
        zodiac = "Leo";
    };
    if(birthday[0]>=24 && birthday[1]==8 || birthday[0]<=23 && birthday[1]==9) {
        zodiac = "Virgo";
    };
    if(birthday[0]>=24 && birthday[1]==9 || birthday[0]<=23 && birthday[1]==10) {
        zodiac = "Libra";
    };
    if(birthday[0]>=24 && birthday[1]==10 || birthday[0]<=22 && birthday[1]==11) {
        zodiac = "Scorpio";
    };
    if(birthday[0]>=23 && birthday[1]==11 || birthday[0]<=21 && birthday[1]==12) {
        zodiac = "Sagittarius";
    };
    alert("Your zodiac is " + zodiac);
    
    let calcChineseNum = birthday[2]%12;
    let chineseAnimal;
    switch(calcChineseNum) {
        case 1: 
            chineseAnimal = "Rooster";
            break;
        case 2: 
            chineseAnimal = "Dog";
            break;
        case 3: 
            chineseAnimal = "Pig";
            break;
        case 4: 
            chineseAnimal = "Rat";
            break;
        case 5: 
            chineseAnimal = "Ox";
            break;
        case 6: 
            chineseAnimal = "Tiger";
            break;
        case 7: 
            chineseAnimal = "Rabbit";
            break;
        case 8: 
            chineseAnimal = "Dragon";
            break;
        case 9: 
            chineseAnimal = "Snake";
            break;
        case 10: 
            chineseAnimal = "Horse";
            break;
        case 11: 
            chineseAnimal = "Goat";
            break;
        default: 
            chineseAnimal = "Monkey";
            break; 
    }  
    alert("Your Chinese animal is " + chineseAnimal);
}
getFullAge();


